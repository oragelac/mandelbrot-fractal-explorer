#include <SDL/SDL.h>
#include <iostream>

#define LARGEUR_ECRAN 800
#define HAUTEUR_ECRAN 600
using namespace std;

struct point {double x, y;};
unsigned char mandelbrot (const point*);
void putPixel (SDL_Surface*, int, int, unsigned long);
unsigned long Color(unsigned char, unsigned char, unsigned char, unsigned char);
SDL_Rect ajuster_position (const point*, const point*, const point*);
void tracer_fractale (SDL_Surface*, const point*, const point*, const double, bool);


int main (int argc, char **argv)
{
	SDL_Event event;
	SDL_Surface *ecran = NULL;
	bool continuer = true, interrupteur = true, calculer = true;
	point borne_inf = {-2, 2}, borne_sup;
	double inc = 0.001;
	const double rapport_ecran = LARGEUR_ECRAN / (double) HAUTEUR_ECRAN;
	
	SDL_Init(SDL_INIT_VIDEO);
	ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, 32, SDL_SWSURFACE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("Fractale de Mandelbrot", NULL);
	
	double d = inc * 100;
	
	while (continuer)
    {
		if (interrupteur)
		{
			SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 255));
			if (SDL_MUSTLOCK(ecran)) SDL_LockSurface(ecran);
			tracer_fractale (ecran, &borne_inf, &borne_sup, inc, calculer);
			if (SDL_MUSTLOCK(ecran)) SDL_UnlockSurface(ecran);
			SDL_Flip(ecran);
			interrupteur = false;
			calculer = false;
		}
		
        SDL_PollEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
            continuer = false;
            break;
			
			case SDL_KEYDOWN:
			if (event.key.keysym.sym == SDLK_ESCAPE) continuer = false;
			
			if (event.key.keysym.sym == SDLK_KP_PLUS)
			{
				borne_sup.x = (borne_inf.x + borne_sup.x) / 2;
				borne_sup.y = (borne_inf.y + borne_sup.y) / 2;
				interrupteur = true;
				inc /= 2.5;
			}
			
			if (event.key.keysym.sym == SDLK_KP_MINUS)
			{
				borne_sup.x += borne_sup.x - borne_inf.x;
				borne_sup.y += borne_sup.y - borne_inf.y;
				interrupteur = true;
				inc *= 2.5;
			}
			
			if (event.key.keysym.sym == SDLK_RIGHT and borne_sup.x <= 2)
			{
				cout << "Deplacement vers la droite" << endl;
				borne_inf.x += d;
				borne_sup.x += d;
				interrupteur = true;
			}
			
			if (event.key.keysym.sym == SDLK_LEFT and borne_inf.x >= -2)
			{
				cout << "Deplacement vers la gauche" << endl;
				borne_inf.x -= d;
				borne_sup.x -= d;
				interrupteur = true;
			}
			
			if (event.key.keysym.sym == SDLK_UP and borne_sup.y >= -2)
			{
				
				cout << "Deplacement vers le haut" << endl;
				borne_inf.y -= d;
				borne_sup.y -= d;
				interrupteur = true;
			}
			
			if (event.key.keysym.sym == SDLK_DOWN and borne_inf.y <= 2)
			{
					cout << "Deplacement vers le bas" << endl;
					borne_inf.y += d;
					borne_sup.y += d;
					interrupteur = true;
			}
			
			break;
        }
    }
	
    SDL_Quit();
	return 0;
}

unsigned char mandelbrot (const point *a)
{
	double x = 0, y = 0, xc, yc, xy;
	unsigned char i;
	for (i = 0; i < 30; ++i)
	{
		xc = x * x;
		yc = y * y;
		xy = x * y;
		if (xc + yc >= 4) break; // Si la suite diverge on sort de la boucle
		x = (xc - yc) + a->x;
		y = (2 * xy) + a->y;
	}
	
	return i;
}

void putPixel(SDL_Surface *surface, int x, int y, unsigned long pixel)
{
	if (x >= 0 and y >= 0 and x < surface->w and y < surface->h)
	{
		int bpp = surface->format->BytesPerPixel;
		unsigned char *p = (unsigned char *)surface->pixels + y * surface->pitch + x * bpp;
		
		switch(bpp) 
		{
			case 1:
			*p = (unsigned char)pixel;
			break;
			
			case 2:
			*(unsigned short*)p = (unsigned short)pixel;
			break;
			
			case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN) 
			{
				p[0] = (unsigned char)((pixel >> 16) & 0xff);
				p[1] = (unsigned char)((pixel >> 8) & 0xff);
				p[2] = (unsigned char)(pixel & 0xff);
			}
				
			else 
			{
				p[0] = (unsigned char)(pixel & 0xff);
				p[1] = (unsigned char)((pixel >> 8) & 0xff);
				p[2] = (unsigned char)((pixel >> 16) & 0xff);
			}
			break;
		
			case 4:
			*(unsigned long*)p = pixel;
			break;
		}
	}
}

unsigned long Color(unsigned char R,unsigned char V,unsigned char B,unsigned char A)
{
	return 65536*R + 256*V + B + 16777216 * A;
}

SDL_Rect ajuster_position (const point *p, const point *origine, const point *fin)
{
	SDL_Rect position;
	point decalage;
	
	decalage.x = -(origine->x);
	decalage.y = -(fin->y);
		
	position.x = (LARGEUR_ECRAN - 1) * ((p->x + decalage.x) / (fin->x - origine->x));
	position.y = (HAUTEUR_ECRAN - 1) * ((p->y + decalage.y) / (origine->y - fin->y));
	
	return position;
}

void tracer_fractale (SDL_Surface *surface, const point *origine, const point *fin, const double inc, bool calculer)
{
	point p;
	unsigned char color;
	SDL_Rect position;
	
	for (p.x = origine->x; p.x < fin->x; p.x += inc)
		{
			for (p.y = origine->y; p.y > fin->y; p.y -= inc)
			{			
				position = ajuster_position (&p, origine, fin);
				
				color = mandelbrot (&p);
				
				if (color == 30) putPixel (surface, position.x, position.y, Color (0, 0, 0, 0));
				
				else putPixel (surface, position.x, position.y, Color (255 - 20 * (255 / color), 255 - 29 * (255 / color), 255, 0));
			}
		}
}
