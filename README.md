# Mandelbrot Fractal Explorer #

J'ai développé ce projet avec l'idée de mettre en pratique mon apprentissage 
de la librairie graphique SDL.  

Il permet de naviguer dans l'[ensemble de Mandelbrot](https://fr.wikipedia.org/wiki/Ensemble_de_Mandelbrot).

L'ensemble de Mandelbrot se présentait commme un très bon choix d'application pour mettre en pratique  
les bases que j'avais acquis de cette librairie graphique; il requérait de comprendre et maîtriser  
un grand nombre de concepts liés à la bibliothèque SDL.


Ce projet m'aura aussi appris à faire plus attention à optimiser le code de mes projets.  
En effet la génération de l'ensemble de Mandelbrot est un processus gourmand en ressources,  
et il aura donc fallu que je sois attentif quant à l'efficacité des algorithmes que j'ai utilisés.


## Configuration requise ##
SDL 1.2

## Comment exécuter le *Mandelbrot Fractal Explorer* ? ##

Pour lancer le logiciel, il suffit simplement de compiler le programme à l'aide d'un compilateur C++ 
et de lier la librairie mathématique et la librairie SDL (version 1.2).

Exemple avec le compilateur g++ :

```
#!bash
g++ main.cpp -lm -lSDL
```

## Comment utiliser ce logiciel ? ##

![mandelbrot.PNG](https://bitbucket.org/repo/jggMgdd/images/1440130798-mandelbrot.PNG)

* Pour zoomer, utilsez les touches + et - du pavé numérique.
* Pour se déplacer, utilisez les touches directionnelles.